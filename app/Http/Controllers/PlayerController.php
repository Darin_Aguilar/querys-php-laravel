<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PlayerController extends Controller
{
    public function index()
    {
        //1
        // $player = DB::table('players')->where('age', 20)->first();
        // return response()->json(["response" => $player -> name]);

        //2
        // $player = DB::table('players')->where('age',20)->value('name');
        // return response()->json(["response" => $player]);

        //3
        // $player = DB::table('players')->find(4);
        // return response()->json(["response" => $player]);

        //especificar columna que la coleccion debe usar como clave
        // $ages = DB::table('players')->pluck('age', 'name');
        // foreach($ages as $age => $result){
        //     echo $result."<br>";
        // }

        //desfragmentacion de informacion 
        //ADVERTENCIA si se cambian los registros de la base de datos los resultados de la fragmentacion podrian cambiar es preferible usar chunk
        // DB::table('user_details')->orderBy('id')->chunk(100, function (Collection $users) {
        //     foreach ($users as $user) {
        //         echo $user->id . "-" . $user->first_name . "<br>";
        //     }
        //     return false;
        // });

        ////actualizar base de datos y hacer lazy load
        // DB::table('user_details')
        //     ->where('gender', 'Femenino')
        //     ->lazyById()
        //     ->each(function (object $user){
        //        //actualizamos genero a español
        //         DB::table('user_details')
        //         ->where('id', $user->id)
        //         ->update(['gender'=> "femenino"]);
        //         echo "ID: $user->id, Género: $user->gender";
        //     });

        //cantidad de registro
        // $players = DB::table('players')->count();
        // return response()->json(["Response"=>$players]);

        //valor mas alto
        // $players = DB::table('players')->max('age');
        // return response()->json(["Response"=>$players]);

        // //valor mas alto
        // $players = DB::table('players')->min('age');
        // return response()->json(["Response"=>$players]);

        // //valor promedio
        // $players = DB::table('players')->avg('age');
        // return response()->json(["Response"=>$players]);

        //suma
        // $players = DB::table('players')->sum('age');
        // return response()->json(["Response"=>$players]);
        

        //cantidad de jugadores femeninos
        $cant_female = DB::table('user_details')
            ->where('gender', 'Femenino')
            ->count();
        return response()->json(["Response"=>$cant_female]);

    }
}
